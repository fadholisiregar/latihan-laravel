<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1 - Berlatih HTML</title>
</head>
<body>
    <form action="welcome">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <p>First name:</p>
        <input type="text" name='nama_depan'>
        <p>Last name:</p>
        <input type="text" name='nama_belakang'>

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>
         
        <p>Nationality:</p>
        <select>
            <option value="Indonesia">Indonesia</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" >
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox">
        <label>English</label><br>
        <input type="checkbox">
        <label >Other</label><br>

        <p>Bio:</p>
        <textarea cols="30" rows="10"></textarea><br>
        <button>Sign Up</button>
    </form>
</body>
</html>