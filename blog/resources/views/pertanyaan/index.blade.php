@extends('adminlte.master')

@section('content')
    <div class="pl-3 pt-3">
        <div class="card">
            <div class="card-header">
                <h2>List Pertanyaan</h2>
            </div>
            <div class="card-body">
            @if(session('sukses'))
                <div class="alert alert-success">
                    {{ session('sukses') }}
                </div>
            @endif
                <a href="/pertanyaan/create" class="btn btn-info mb-2">Buat Pertanyaan</a>
                <table class="table table-bordered">
                    <thead>                  
                    <tr>
                        <th style="width: 10px">No</th>
                        <th>Judul</th>
                        <th>Pertanyaan</th>
                        <th style="width: 40px">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($pertanyaan as $key => $item)
                            <tr>
                                <td> {{ $key + 1 }} </td>
                                <td> {{ $item->judul }} </td>
                                <td> {{ $item->isi }} </td>
                                <td style="display: flex">
                                    <a href="/pertanyaan/{{$item->id}}" class="btn btn-info btn-sm">show</a>
                                    <a href="/pertanyaan/{{$item->id}}/edit" class="btn btn-default btn-sm">edit</a>
                                    <form action="/pertanyaan/{{$item->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="delete" class="btn btn-danger">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" align="center">Tidak ada pertanyaan</td>
                            </tr> 
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection